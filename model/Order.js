const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product', required: true },
    status: { type: Boolean, default: false },
    ordered_by : {type: mongoose.Schema.Types.ObjectId, ref:"User", required: true }
});

module.exports = mongoose.model('Order', orderSchema);