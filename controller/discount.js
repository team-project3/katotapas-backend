const router = require("express").Router();
const Discount = require("../model/Discount");
const coupongenerator = require("../coupongenerator");

// Create a discount
router.post("/", async (req, res, next) => {
  var myDiscountCode = coupongenerator();

  //var testcode = "";

  const mydiscount = await Discount.findOne({ code: myDiscountCode });
  if (mydiscount) {
    return res.status(400).send("This code already exists");
  }

  const discount = new Discount({
    code: myDiscountCode,
    isPercent: req.body.isPercent,
    expireDate: req.body.expireDate,
    amount: req.body.amount,
    isActive: true,
  });

  try {
    const savedDiscount = await discount.save();
    console.log("Successfully created discount code");
    res.json({ discount });
  } catch (err) {
    console.log(err);
    res.status(400).json(err);
  }
});

// Retrieve all Discounts from the database.
exports.findAll = (req, res) => {};

// Find a single discount with an id
exports.findOne = (req, res) => {};

// Delete a discount with the specified id in the request
router.delete("/:discountId", (req, res, next) => {
    const id = req.params.discountId;
    Discount.remove({ _id: id })
      .exec()
      .then(result => {
        res.status(200).json({
            message: 'Code deleted',
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });

module.exports = router;
