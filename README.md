<p align="center">
  <a href="https://gitlab.com/team-project3/katotapas"><img width="341" height="63" src="https://i.imgur.com/hNfHyiP.png"></a>
</p>

# Katotapas Restaurant

## Topic of the project :
As a Team we have decided to focus our project on a takeaway / food delivery service for our made-up restaurant called KatoTapas. We will be building a website that showcases the specific menu of the restaurant, being able to browse through all of the different options and see their prices. The client will be able to choose between these options and pick the number of items of each product chosen to add it to their ‘cart’. After adding all the products, they will choose if they want it for take away or home delivery:
* For Take Away the client will need to choose a desired pick-up time and give some basic information like their name, and also decide if they want to pay by cash or card so the pick-up is faster.
* For Home Delivery the client will need to insert an address, and also decide if they want to pay by cash or card so the delivery person can bring the necessary change or card reader.
There will be an extra text input at the end of the process in case the customer wants to add a specific request for their order.
We also intend to add different offers or promotional codes, like 2x1 or similar, to make sure the client feels rewarded for being a customer. As an extra, for each item selected the customer will be allowed to customize their order, for example ‘remove cheese from my hamburger’ or ‘extra spicy’.


## Features
* Authentication to save orders (repeat last order) & see status
  * Store address, personal information, orders
* Landing page:
  * Restaurant contact information (opening hours)
    * Location on map
    * Social media
  * Restaurant general information
* Menu -> each item can be added to the order
* Cart with all items picked & the amount
* Choose between take-away or home delivery
* Pay by cash or credit card
* Choose pickup time (takeaway) or address (home delivery)
* Promotion / discount codes
* Order status
* Preorder / close ordering depending on opening hours

## Project management
* [Trello](https://trello.com/b/0Kw4QkvN/incs2-sqe-team-project)
