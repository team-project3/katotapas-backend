const app = require("../app");
const mongoose = require('mongoose');
const request = require("supertest");

/*
    declare the token variable in a scope accessible
    by the entire test suite
*/

describe("GET /", () => {
  // token not being sent - should respond with a 401
  test("It should require authorization", () => {
    return request(app)
      .get("/api/post")
      .then((response) => {
        expect(response.statusCode).toBe(401);
      });
  });
  // send the token - should respond with a 200
  test("It responds with JSON", () => {
    return request(app)
      .get("/api/post")
      .set("auth-token",process.env.TOKEN_TEST)
      .then((response) => {
        expect(response.statusCode).toBe(200);
        expect(response.type).toBe("application/json");
      });
  });
});
