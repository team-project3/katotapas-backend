const app = require('../app');
const mongoose = require('mongoose');
const request = require('supertest')

const users = require('../model/User')

beforeEach(async() =>{
    await users.deleteMany({})
});

test("POST /api/user/register", async () => {
    const data = { name: "Ronald Koeman", email: "rkoeman@fcbarcelona.cat", password: "12345678" };
  
    await request(app).post("/api/user/register")
      .send(data)
      .expect(200)
  }, 30000);

  test("POST /api/user/register", async () => {
    const data = { name: "Lomé Saka", email: "lomesaka@edupl.com", password: "1234567" };
  
    await request(app).post("/api/user/register")
      .send(data)
      .expect(400)
  }, 30000);

  test("POST /api/user/register", async () => {
    const data = { name: "Lo", email: "loa@edu.com", password: "12345678" };
  
    await request(app).post("/api/user/register")
      .send(data)
      .expect(400)
  }, 30000);