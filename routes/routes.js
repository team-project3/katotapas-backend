const router = require('express').Router();

//Authentication
const authRoute = require('../controller/auth');
router.use('/api/user', authRoute);

//Any with middleware
const postRoute = require('../controller/post');
router.use('/api/post', postRoute);

//Products
const productRoute = require('../controller/product');
router.use('/api/product', productRoute);

//Orders
const orderRoute = require('../controller/order');
router.use('/api/order', orderRoute);

//Discounts
const discountRoute = require('../controller/discount');
router.use('/api/discount', discountRoute);

module.exports = router;